## SmartDeploy Email Dev
```
Important!  
These templates are no longer 100% up to date. The files in `\SmartDeploy.WebApp\EmailTemplates` have C# tokens in them, so don't overwrite those.
```

These emails are based off of two existing Marketo templates:
* [Marketing Style 1](https://bitbucket.org/salter-smartdeploy/marketing-style-1-email/src/master/)
* [Sales Alert](https://bitbucket.org/salter-smartdeploy/sales-alert-email/src/master/)

### Getting Started

1. Install [Node.js with npm](https://nodejs.org/en/) and [Gulp.js](http://gulpjs.com/)
2. Open a command line tool and navigate to this directory
3. Run `npm install` to download all the required node modules
4. Run `gulp` to start watching your files for changes, or `gulp thetaskname` to run a single task

### Gulp Tasks

| Task Name  |  Task Purpose |
|---|---|
| `gulp inlineCSS`  |  Create a new html file with all non-media query CSS inlined  |


### Styles

Styles are based off of Foundation's [Ink Templates](https://foundation.zurb.com/emails/email-templates.html)

Some styles like *Media Queries and hover/focus states need to go in the src/html files*.  
The CSS inliner task can't inject them if they come from a linked CSS file. See this [inliner media query issue](https://github.com/jonkemp/gulp-inline-css/issues/23).